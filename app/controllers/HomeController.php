<?php

class HomeController extends BaseController {

    public function getIndex() {
        return View::make("home");
    }

    public function getProjectList() {
        $talents = Talent::all();
        $projects = Project::all();
        $talent_skills = [];

        foreach ($talents as $talent) {
            $skills = $talent->skills;
            //only add talents that have skills
            if (!$skills->isEmpty()) {
                $talent_skills[] = ["talent" => $talent, "skills" => $skills];
            }
        }
        $data = [
            "projects" => $projects,
            "talent_skills" => $talent_skills,
        ];
        //add constantScroll to html element- to keep scrollbar when using isotope sorting
        return View::make("projectList")->with($data);
    }

    public function getProject($id) {
        $project = Project::find($id);
        $talent = $project->talent;

        $data = [
            "project" => $project,
            "talent" => $talent
        ];
        return View::make("project", $data);
    }
    
    public function getContact(){
        return View::make("contact");
    }

    public function getAbout(){
        return View::make("about");
    }

}
